const { random } = require('lodash');


const DICT = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
const DICT_SIZE = DICT.length;

exports.uid = function uid(max) {
  let res = '';
  while (max--) res += DICT[random(0, DICT_SIZE - 1)];
  return res;
};
