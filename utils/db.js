const crypto = require('crypto');
const { Client } = require('pg');

const { DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASSWORD } = require('../config');

const db = new Client({
  user: DB_USER,
  password: DB_PASSWORD,
  host: DB_HOST,
  port: DB_PORT,
  database: DB_NAME
});


const usersData = [
  { name: 'user', pwd: 'RbMGbb2)#saGp3Cd#Wu4Zo3$' }
];

function createUsers() {
  const query = 'CREATE TABLE public.users ( ' +
    'name character varying(16) NOT NULL, ' +
    'pwd character varying(128) NOT NULL, ' +
    'PRIMARY KEY (name)' +
  ');';
  console.log('Create users table\n', query);
  return db.query(query)
    .then(() => {
      return Promise.all(usersData.map(({name, pwd}) => {
        return db.query('INSERT INTO users(name, pwd) VALUES($1,$2);', [name, crypto.createHash('sha1').update(pwd).digest('hex')]);
      }))
    })
    .catch(err => console.log(err))
}

function createClients() {
  const query = 'CREATE TABLE public.clients ( ' +
    'name character varying(16) NOT NULL, ' +
    'client_id character varying(16) NOT NULL, ' +
    'user_id character varying(16) NOT NULL, ' +
    'client_secret character varying(32) NOT NULL, ' +
    'PRIMARY KEY (client_id)' +
  ');';
  console.log('Create clients table\n', query);
  return db.query(query)
    .catch(err => console.log(err))
}

function createAccessTokens() {
  const query = 'CREATE TABLE public.access_tokens ( ' +
    'token character varying(256) NOT NULL, ' +
    'user_id character varying(16) NOT NULL, ' +
    'client_id character varying(16) NOT NULL, ' +
    'PRIMARY KEY (token)' +
  ');';
  console.log('Create access_tokens table\n', query);
  return db.query(query)
    .catch(err => console.log(err))
}

function createRefreshTokens() {
  const query = 'CREATE TABLE public.refresh_tokens ( ' +
    'refresh_token character varying(256) NOT NULL, ' +
    'user_id character varying(16) NOT NULL, ' +
    'client_id character varying(16) NOT NULL, ' +
    'PRIMARY KEY (refresh_token)' +
    ');';
  console.log('Create refresh_tokens table\n', query);
  return db.query(query)
    .catch(err => console.log(err))
}

function createPhones() {
  const query = 'CREATE TABLE public.phones ( '+
    'phone character varying(22) NOT NULL, '+
    'PRIMARY KEY (phone) '+
  ');';
  console.log('Create phones table\n', query);
  return db.query(query)
    .catch(err => console.log(err))
}


function existTable(table) {
  const query = 'SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema=$1 AND table_name=$2);';
  return db.query(query, ['public', table])
    .then(res => {
      console.log('Table', table, res.rows[0].exists ? '' : 'not', 'exists');
      return res.rows[0].exists;
    })
}

db.connect()
  .then(() => Promise.all([
    existTable('users').then(exists => !exists && createUsers()),
    existTable('clients').then(exists => !exists && createClients()),
    existTable('phones').then(exists => !exists && createPhones()),
    existTable('access_tokens').then(exists => !exists && createAccessTokens()),
    existTable('refresh_tokens').then(exists => !exists && createRefreshTokens())
  ]))
  .catch(error => console.error('Connect error', error));

module.exports = db;