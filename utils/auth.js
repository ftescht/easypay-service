const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const BasicStrategy = require('passport-http').BasicStrategy;
const ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
const BearerStrategy = require('passport-http-bearer').Strategy;

const accessTokens = require('../stores/accessTokens');
const clients = require('../stores/clients');
const users = require('../stores/users');


passport.serializeUser(function(user, callback) {
  callback(null, user.name);
});

passport.deserializeUser(function(id, callback) {
  users.getItem(id)
    .then(user => {
      if (!user) throw new Error('User not found');
      callback(null, user);
    })
    .catch(error => callback(error));
});


passport.use(new LocalStrategy(function(username, password, callback) {
  users.verifyPassword(username, password)
    .then(user => {
      if (!user) return callback(null, false);
      callback(null, user);
    })
    .catch(error => callback(error));
}));


passport.use('clientBasic', new BasicStrategy(function (client_id, client_secret, callback) {
  clients.getItem(client_id)
    .then(client => {
      if (!(client || client.secret === client_secret)) return callback(null, false);
      callback(null, client);
    })
    .catch(error => callback(error));
}));

passport.use('clientPassword', new ClientPasswordStrategy(function (client_id, client_secret, callback) {
  clients.getItem(client_id)
    .then(client => {
      if (!client || client.client_secret !== client_secret) return callback(null, false);
      callback(null, client);
    })
    .catch(error => callback(error));
}));


passport.use('accessToken', new BearerStrategy(function (accessToken, callback) {
  const accessTokenHash = crypto.createHash('sha1').update(accessToken).digest('hex');
  accessTokens.getItem(accessTokenHash)
    .then(accessToken => {
      if (!accessToken) return callback(null, false);
      if (!accessToken.user_id) {
        return accessTokens.removeItem(accessTokenHash).then(() => Promise.reject(new Error('Bad token')));
      }
      if (new Date() > accessToken.expirationDate) {
        return accessTokens.removeItem(accessTokenHash).then(() => Promise.reject(new Error('Bad token')));
      }

      return users.getItem(accessToken.user_id).then(user => {
        if (!user) throw new Error('Tokens user not found');
        callback(null, user, { scope: accessToken.scope || '*' });
      })
    })
    .catch(error => callback(error));
}));
