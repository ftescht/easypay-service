const oauth2orize = require('oauth2orize');
const passport = require('passport');
const crypto = require('crypto');
const { uid } = require('./lib');

const accessTokens = require('../stores/accessTokens');
const refresh_tokens = require('../stores/refreshTokens');
const accessCodes = require('../stores/accessCodes');
const clients = require('../stores/clients');


const server = oauth2orize.createServer();

server.serializeClient(function(client, callback) {
  console.log('serializeClient', client);
  callback(null, client.client_id);
});
server.deserializeClient(function(id, callback) {
  clients.getItem(id)
    .then(client => {
      if (!client) throw new Error('Client not found');
      callback(null, client);
    })
    .catch(error => callback(error));
});

server.grant(oauth2orize.grant.code(function(client, redirectUri, user, ares, callback) {
  const code = uid(16);
  const authCode = {
    code: crypto.createHash('sha1').update(code).digest('hex'),
    client_id: client.client_id,
    user_id:   client.user_id,
    scope:    ares.scope,
    redirectUri
  };
  accessCodes.saveItem(authCode)
    .then(authCode => callback(null, code))
    .catch(error => callback(error));
}));

server.exchange(oauth2orize.exchange.code(function(client, code, redirectUri, callback) {
  accessCodes.getItem(crypto.createHash('sha1').update(code).digest('hex'))
    .then(accessCode => {
      if (!accessCode) return callback(null, false);
      if (client.client_id !== accessCode.client_id) return callback(null, false);
      if (redirectUri !== accessCode.redirectUri) return callback(null, false);

      return accessCodes.removeItem(code).then(() => {
        const token = uid(256);
        const refresh_token = uid(256);
        const tokenHash = crypto.createHash('sha1').update(token).digest('hex');
        const refresh_tokenHash = crypto.createHash('sha1').update(refresh_token).digest('hex');
        const expirationDate = new Date(new Date().getTime() + (3600 * 1000));

        return Promise.all([
          accessTokens.saveItem({
            token: tokenHash,
            user_id: accessCode.user_id,
            client_id: accessCode.client_id,
            expirationDate
          }),
          refresh_tokens.saveItem({
            refresh_token: refresh_tokenHash,
            user_id: accessCode.user_id,
            client_id: accessCode.client_id,
            expirationDate
          })
        ]).then(() => callback(null, token, refresh_token, {expires_in: expirationDate}));
      });
    })
    .catch(error => callback(error));
}));

server.exchange(oauth2orize.exchange.refreshToken(function (client, refresh_token, scope, callback) {
  const refresh_tokenHash = crypto.createHash('sha1').update(refresh_token).digest('hex');
  accessCodes.getItem(refresh_tokenHash)
    .then(accessToken => {
      if (!accessToken) return callback(null, false);
      if (client.client_id !== accessToken.client_id) return callback(null, false);

      const newAccessToken = uid(256);
      const accessTokenHash = crypto.createHash('sha1').update(newAccessToken).digest('hex');
      const expirationDate = new Date(new Date().getTime() + (3600 * 1000));

      return accessTokens.getByUser(accessToken.user_id)
        .then(tokens => Promise.all(tokens.map(({token}) => accessTokens.removeItem(token))))
        .then(() => accessTokens.saveItem({
          token: accessTokenHash,
          user_id: accessToken.user_id,
          client_id: accessToken.client_id,
          scope,
          expirationDate
        }))
        .then(() => callback(null, newAccessToken, refresh_token, {expires_in: expirationDate}))
    })
    .catch(error => callback(error));
}));

exports.authorization = [
  function(req, res, next) {
    const { client_id, redirect_uri, response_type } = req.query;
    if (!req.user) {
      return res.render('loginOauth', { client_id, redirect_uri, response_type });
    }
    return next();
  },
  server.authorization(function(client_id, redirectUri, callback) {
    clients.getItem(client_id)
      .then(client => {
        if (!client) throw new Error('Client not found');
        callback(null, client, redirectUri);
      })
      .catch(error => callback(error));
  }),
  function(req, res){
    res.render('decision', { transactionID: req.oauth2.transactionID, user: req.user, client: req.oauth2.client });
  }
];

exports.authorize = function(req, res) {
  const { response_type, redirect_uri, client_id } = req.body;
  res.redirect('/oauth/authorization?response_type='+ response_type +'&client_id='+ client_id +'&redirect_uri='+ redirect_uri)
};

exports.decision = [
  function(req, res, next) {
    if (req.user) next();
    else res.redirect('/oauth/authorization')
  },
  server.decision()
];

exports.token = [
  passport.authenticate(['clientBasic', 'clientPassword'], { session: false }),
  server.token(),
  server.errorHandler()
];

