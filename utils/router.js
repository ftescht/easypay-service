const express = require('express');
const passport = require('passport');

const { authorization, authorize, decision, token } = require('./oauth2');
const { loginLocal } = require('../routes/auth');

const router = express.Router();

const { getClients, registerClient } = require('../routes/clients');
const { getPhones, addPhone, removePhone, isPhoneExist } = require('../routes/phones');


function _send(data, status) {
  return function (req, res) {
    res.send(data, status);
  }
}
function _render(template, params = {}) {
  return function (req, res) {
    res.render(template, params);
  }
}

function corsMid(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
}

function checkUserRedirect(req, res, next) {
  if (req.user) return next();
  res.redirect('/authorization');
}

function checkUserAccess(req, res, next) {
  if (req.user) return next();
  res.send('Bad user', 403);
}

const localCheck = passport.authenticate('local', { failureRedirect: '/authorization' });
const oauthCheck = passport.authenticate('accessToken', { session: false });


router.use(corsMid);


// WEB

router.route('/clients').get(checkUserRedirect, getClients);
router.route('/api/phones').get(checkUserRedirect, getPhones);


// API

router.route('/api/ping').post(oauthCheck, checkUserAccess, (req, res) => res.send(''+Date.now()));

router.route('/api/phones').post(oauthCheck, checkUserAccess, getPhones);
router.route('/api/phoneAdd').post(oauthCheck, checkUserAccess, addPhone);
router.route('/api/phoneRemove').post(oauthCheck, checkUserAccess, removePhone);
router.route('/api/phoneExist').post(oauthCheck, checkUserAccess, isPhoneExist);


// Registration

router.route('/client/registration')
  .get(checkUserRedirect, _render('clientRegistration'))
  .post(checkUserAccess, registerClient);


// Authorization

router.route('/authorization')
  .get(_render('loginLocal'))
  .post(localCheck, loginLocal);

router.route('/oauth/authorization')
  .get(authorization)
  .post(function (req, res, next) {
    passport.authenticate('local', function(err, user) {
      if (err) return next(err);
      req.logIn(user, () => authorize(req, res));
    })(req, res, next);
  });

router.route('/oauth/token').post(token);
router.route('/decision').post(decision);
router.route('/restricted').get(oauthCheck, _send('You successfully accessed the restricted resource'));


// App
router.route('/').get(checkUserRedirect, _render('index'));


module.exports = router;