const { isObject } = require('lodash');
const { isEmpty, isLength } = require('validator');
const { uid } = require('../utils/lib');

const clients = require('../stores/clients');

function getClients(req, res) {
  clients.getItemsByUser(req.user.name)
    .then(clients => res.json(clients))
    .catch(error => res.send(error));
}

function registerClient(req, res) {
  if (!req.user) return res.send('Bad user', 403);
  if (!isObject(req.body)) return res.send('Empty request body', 500);

  const { name } = req.body;
  if (isEmpty(name)) return res.send('Empty client name', 500);
  if (!isLength(name, 3, 40)) return res.send('Client name not valid', 500);

  const client_id = uid(16);
  const client_secret = uid(32);

  const clientDoc = {
    name,
    client_id,
    client_secret,
    user_id: req.user.name
  };

  clients.getItem(client_id)
    .then(client => {
      if (client) return res.send('Name is already taken', 422);
      return clients.saveItem(clientDoc).then(() => res.send(clientDoc, 201));
    })
    .catch(error => res.send(error));
}

exports.getClients = getClients;
exports.registerClient = registerClient;