const { isObject } = require('lodash');
const { isEmpty, isMobilePhone } = require('validator');
const phones = require('../stores/phones');

function checkPhoneMid(req, res, next) {
  if (!isObject(req.body)) return res.status(500).send('Empty request body');

  const { phone } = req.body;
  if (isEmpty(phone)) {
    res.status(500).send('Empty phone');
  } else if (!isMobilePhone(phone, 'ru-RU')) {
    res.status(500).send('Phone not valid');
  } else {
    next();
  }
}

function getPhones(req, res) {
  phones.getItems()
    .then(result => {
      res.json({ ok: true, result });
    })
    .catch(error => {
      console.error(error);
      res.json({ ok: false, error: error.detail }, 500);
    })
}

function removePhone(req, res) {
  const { phone } = req.body;
  phones.removeItem(phone)
    .then(result => {
      res.json({ ok: true, result });
    })
    .catch(error => {
      console.error(error);
      res.json({ ok: false, error: error.detail }, 500);
    })
}

function addPhone(req, res) {
  const { phone } = req.body;
  phones.saveItem(phone)
    .then(result => {
      res.json({ ok: true, result });
    })
    .catch(error => {
      console.error(error);
      res.json({ ok: false, error: error.detail }, 500);
    })
}

function isPhoneExist(req, res) {
  res.json({ ok: true });
}

exports.getPhones = getPhones;
exports.removePhone = [checkPhoneMid, removePhone];
exports.addPhone = [checkPhoneMid, addPhone];
exports.isPhoneExist = [checkPhoneMid, isPhoneExist];
