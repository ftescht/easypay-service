function loginLocal(req, res) {
  if (req.user) return res.redirect('/');
  res.render('loginLocal', { message: 'Bad login' });
}

exports.loginLocal = loginLocal;
