const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const session = require('express-session');
require('./utils/auth');

const { APP_PORT, SECRET_KEY } = require('./config');

const app = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'pug');

app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
  secret: SECRET_KEY,
  saveUninitialized: true,
  resave: true
}));
app.use(passport.initialize());
app.use(passport.session());

// Router
app.use(require('./utils/router'));


app.listen(APP_PORT, () => {
  console.log('server-api listening on port '+ APP_PORT);
});