module.exports.APP_PORT = +(process.env.APP_PORT || 9000);

module.exports.DB_HOST = process.env.DB_HOST || 'localhost';
module.exports.DB_PORT = +(process.env.DB_PORT || process.env.DB_PORT || 5432);
module.exports.DB_NAME = process.env.POSTGRES_DB || process.env.DB_NAME || 'app';
module.exports.DB_USER = process.env.POSTGRES_USER || process.env.DB_USER || 'ftescht';
module.exports.DB_PASSWORD = process.env.POSTGRES_PASSWORD || process.env.DB_PASSWORD || 'ftescht';

module.exports.SECRET_KEY = process.env.SECRET_KEY || 'SessionKey1SessionKey2';