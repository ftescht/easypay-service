const store = {};


function getItems() {
  return Promise.resolve(Object.keys(store).map(id => store[id]));
}

function getItem(id) {
  if (!id) Promise.reject(new Error('Bad id'));
  return Promise.resolve(store[id]);
}

function saveItem(doc) {
  store[doc.code] = doc;
  return Promise.resolve(doc);
}

function removeItem(id) {
  delete store[id];
  return Promise.resolve();
}


exports.getItems = getItems;
exports.getItem = getItem;
exports.saveItem = saveItem;
exports.removeItem = removeItem;
