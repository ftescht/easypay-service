const db = require('../utils/db');

function cleanPhone(phone) {
  return phone.replace(/\D/g, '');
}

function getItems() {
  return db.query('SELECT * FROM phones ORDER BY phone ASC;').then(res => res.rows.map(row => row.phone));
}

function saveItem(phone) {
  return db.query('INSERT INTO phones(phone) VALUES($1);', [cleanPhone(phone)]).then(res => phone);
}

function removeItem(phone) {
  return db.query('DELETE FROM phones WHERE phone=($1);', [cleanPhone(phone)]);
}


exports.getItems = getItems;
exports.saveItem = saveItem;
exports.removeItem = removeItem;
