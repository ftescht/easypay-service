const db = require('../utils/db');

function getItem(token) {
  if (!token) Promise.reject(new Error('Bad token'));
  return db.query('SELECT * FROM refresh_tokens WHERE token=($1);', [token]).then(res => res.rows[0]);
}

function saveItem(doc) {
  return db.query('INSERT INTO refresh_tokens(refresh_token, user_id, client_id) VALUES($1, $2, $3);', [doc.refresh_token, doc.user_id, doc.client_id])
    .then(res => doc);
}

function removeItem(token) {
  return db.query('DELETE FROM refresh_tokens refresh_token=($1);', [token]);
}

function getByUser(user_id) {
  if (!user_id) Promise.reject(new Error('Bad user_id'));
  return db.query('SELECT * FROM refresh_tokens WHERE user_id=($1);', [id]).then(res => res.rows);
}

exports.getItem = getItem;
exports.saveItem = saveItem;
exports.removeItem = removeItem;
exports.getByUser = getByUser;
