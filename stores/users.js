const crypto = require('crypto');
const { isObject, omit } = require('lodash');
const db = require('../utils/db');

function _cleanItem(item) {
  return item && isObject(item) ? omit(item, ['pwd']) : null;
}

function getItem(id, full = false) {
  if (!id) Promise.reject(new Error('Bad id'));
  return db.query('SELECT * FROM users WHERE name=($1);', [id]).then(res => {
    const user = res.rows[0];
    return !user || full ? user : _cleanItem(user);
  });
}

function verifyPassword(id, password) {
  return getItem(id, true).then(user => user && user.pwd === crypto.createHash('sha1').update(password).digest('hex') ? user : null);
}

exports.getItem = getItem;
exports.verifyPassword = verifyPassword;
