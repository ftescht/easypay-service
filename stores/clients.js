const db = require('../utils/db');


function getItem(id) {
  if (!id) Promise.reject(new Error('Bad id'));
  return db.query('SELECT * FROM clients WHERE client_id=($1);', [id]).then(res => res.rows[0]);
}

function saveItem(doc) {
  return db.query('INSERT INTO clients(name, user_id, client_id, client_secret) VALUES($1, $2, $3, $4);', [doc.name, doc.user_id, doc.client_id, doc.client_secret])
    .then(res => doc);
}

function getItemsByUser(user_id) {
  return db.query('SELECT * FROM clients WHERE user_id=($1);', [user_id]).then(res => res.rows);
}


exports.getItem = getItem;
exports.saveItem = saveItem;
exports.getItemsByUser = getItemsByUser;